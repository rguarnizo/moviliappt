package com.example.moviliapp;

import com.android.volley.toolbox.StringRequest;

import java.io.Serializable;

public class User implements Serializable {

    private String Name;
    private String LastName;
    private String Email;
    private String Password;
    private String Telephone;
    private String Cedula;
    private String BornDate;
    private String Work;
    private String Grade;
    private String Estrato;



    public User(String name, String lastName, String email, String password, String telephone, String cedula, String bornDate) {
        Name = name;
        LastName = lastName;
        Email = email;
        Password = password;
        Telephone = telephone;
        Cedula = cedula;
        BornDate = bornDate;
    }

    public User(String name, String lastName, String email, String password, String cedula, String bornDate) {
        Name = name;
        LastName = lastName;
        Email = email;
        Password = password;
        Cedula = cedula;
        BornDate = bornDate;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getTelephone() {
        return Telephone;
    }

    public void setTelephone(String telephone) {
        Telephone = telephone;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String cedula) {
        Cedula = cedula;
    }

    public String getBornDate() {
        return BornDate;
    }

    public void setBornDate(String bornDate) {
        BornDate = bornDate;
    }
}
