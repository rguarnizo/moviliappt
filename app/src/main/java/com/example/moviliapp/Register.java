package com.example.moviliapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.moviliapp.DataStructures.Array.Array;
import com.example.moviliapp.DataStructures.Array.DynamicArray;
import com.example.moviliapp.DataStructures.LinkedList;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;


public class Register extends AppCompatActivity {

    private EditText Name,LastName,Email,Password,Cedula,Telefono,BornDate;
    public LinkedList<User> listaUsuarios;
    public DynamicArray<User> listadinamicaUsuarios;
    private ArrayList<User> listaJUsuarios = new ArrayList<>();


    DatabaseReference fireBaseDataBase;

    FirebaseAuth Auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Name = (EditText) findViewById(R.id.editTextName);
        LastName = (EditText) findViewById(R.id.editTextLastName);
        Email = (EditText) findViewById(R.id.editTextEmail);
        Password = (EditText) findViewById(R.id.editPassword);
        Cedula = (EditText) findViewById(R.id.editTextCedula);
        Telefono = (EditText) findViewById(R.id.editTextPhone);
        BornDate = (EditText) findViewById(R.id.BornDate);

        InitFireBase();
        Auth = FirebaseAuth.getInstance();
        recibirDatos();

    }

    private void recibirDatos(){
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            listaUsuarios = (LinkedList<User>) extras.getSerializable("listaUsuarios");
            listadinamicaUsuarios = (DynamicArray<User>) extras.getSerializable("listaDinamicaUsuarios");
            listaJUsuarios = (ArrayList<User>) extras.getSerializable("listaJavaUsuarios");
        }
    }

    public void Login(View view){

        Toast.makeText(this, createUser(), Toast.LENGTH_SHORT).show();


    }

    public String createUser() {

        String name = Name.getText().toString();
        String lastName = LastName.getText().toString();
        String email = Email.getText().toString();
        String password = Password.getText().toString();
        String cedula = Cedula.getText().toString();
        String telefono = Telefono.getText().toString();
        String bornDate = BornDate.getText().toString();

        if((!name.isEmpty() && !lastName.isEmpty() && !email.isEmpty() && !password.isEmpty() && !cedula.isEmpty() && !bornDate.isEmpty()) || !telefono.isEmpty()) {

            Auth.createUserWithEmailAndPassword(email,password);

            User user = new User(name,lastName,email,password,cedula,bornDate);
          /*  for(int i = 0; i < 500000; i++) {
                listaUsuarios.pushBack(user);
                fireBaseDataBase.child("Users").child(user.getCedula()).setValue(user);
            }
            */
           /* for(int i = 0; i < 50000; i++) {
                listadinamicaUsuarios.push(user);
                fireBaseDataBase.child("Users").child(user.getCedula()).setValue(user);
            }*/
            for(int i = 0; i < 500000; i++) {
                listaJUsuarios.add(user);
                fireBaseDataBase.child("Users").child(user.getCedula()).setValue(user);
            }


            return "Usuario creado con exito";
        }else{
            if(name.isEmpty()) Name.setError("Obligatory");
            if(lastName.isEmpty()) LastName.setError("Obligatory");
            if(email.isEmpty()) Email.setError("Obligatory");
            if(password.isEmpty()) Password.setError("Obligatory");
            if(cedula.isEmpty()) Cedula.setError("Obligatory");
            if(bornDate.isEmpty()) BornDate.setError("Obligatory");

            return "No se creo el usuario verifique los campos";
        }


    }

    private void InitFireBase()
    {
        FirebaseApp.initializeApp(this);
        fireBaseDataBase = FirebaseDatabase.getInstance().getReference();
    }

    public void Salir(View view){
        //Toast.makeText(this, String.valueOf(listaUsuarios.getSize()),Toast.LENGTH_SHORT).show();
        Toast.makeText(this, String.valueOf(listadinamicaUsuarios.getSize()),Toast.LENGTH_SHORT).show();


        Intent intent = new Intent(Register.this,MainActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("listaUsuarios",listaUsuarios);
        bundle.putSerializable("listaDinamicaUsuarios",listadinamicaUsuarios);
        intent.putExtras(bundle);
        setResult(1,intent);
        finish();
    }
}
