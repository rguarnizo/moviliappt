package com.example.moviliapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.moviliapp.DataStructures.Array.DynamicArray;
import com.example.moviliapp.DataStructures.LinkedList;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private EditText userText;
    private EditText passwordText;
    private TextView result;
    //private LinkedList<User> userLinkedList = new LinkedList<User>();
    public LinkedList<User> userLinkedList = new LinkedList<>();
    public DynamicArray<User> userDynamicArray = new DynamicArray<>(10);

    private java.util.ArrayList<User> userJArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userText = (EditText) findViewById(R.id.userText);  //R es un puente de comunicación para parte gráfica y lógica.
        passwordText = (EditText) findViewById(R.id.passwordText);

    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    // Recibir Objetos de una Actividad
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bundle bundle = data.getExtras();
        if(bundle != null) {
            userLinkedList = (LinkedList<User>) bundle.getSerializable("listaUsuarios");
            userDynamicArray = (DynamicArray<User>) bundle.getSerializable("listaDinamicaUsuarios");
        }

    }

    public void Login(View view){ //View Permiten dibujar componentes.

        String userName = String.valueOf(userText.getText());
        String userPassword = String.valueOf(passwordText.getText());

        if(userPassword.equals("Admin") && userName.equals("Admin")){
            Intent DataStructuresIntent = new Intent(this,DataStructuresActivity.class);

            startActivity(DataStructuresIntent);
        }

        Toast.makeText(this,String.valueOf(userDynamicArray.getSize()),Toast.LENGTH_SHORT).show();
    }

    public  void Register(View view){
        Intent register = new Intent(MainActivity.this,Register.class);
        /////////////////////////////////////////////
        //MANDAR OBJETOS A OTRO INTENT
        Bundle bundle = new Bundle();
        bundle.putSerializable("listaUsuarios",userLinkedList);
        bundle.putSerializable("listaDinamicaUsuarios",userDynamicArray);
        bundle.putSerializable("listaJavaUsuarios",userJArrayList);
        register.putExtras(bundle);
        // IMPORTANTE
        //////////////////////////////////////////
        startActivityForResult(register,1); // INICIAR ACTIVIDAD PARA RECIBIR UN RESULTADO.

    }
}