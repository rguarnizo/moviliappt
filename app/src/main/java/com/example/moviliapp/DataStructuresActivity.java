package com.example.moviliapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class DataStructuresActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_structures_activity);
    }

    public void Stack(View view){
        Intent stack = new Intent(this,ImplementHeap.class);
        startActivity(stack);
    }
}
