package com.example.moviliapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moviliapp.DataStructures.Node;


public class ImplementHeap extends AppCompatActivity {

    private TextView pilaText;
    final Handler handler = new Handler();
    private Button Lstack1;
    private Button Lstack2;
    private Button Lstack3;
    private Button Lstack4;
    private Button Lstack5;

    private Button Array1;
    private Button Array2;
    private Button Array3;
    private Button Array4;
    private Button Array5;

    private EditText elementText;
    public LinkedStack<String> linkedStack = new LinkedStack<String>();
    public ArrayStack<String> arrayStack = new ArrayStack<>(5);
    int sizeOfLS;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_implement_heap);

        crateButtons();
    }

    public void crateButtons(){
        pilaText = (TextView) findViewById(R.id.pilaText);
        Lstack1 = (Button) findViewById(R.id.Stack1);
        Lstack2 = (Button) findViewById(R.id.Stack2);
        Lstack3 = (Button) findViewById(R.id.Stack3);
        Lstack4 = (Button) findViewById(R.id.Stack4);
        Lstack5 = (Button) findViewById(R.id.Stack5);

        Array1 = (Button) findViewById(R.id.Array1);
        Array2 = (Button) findViewById(R.id.Array2);
        Array3 = (Button) findViewById(R.id.Array3);
        Array4 = (Button) findViewById(R.id.Array4);
        Array5 = (Button) findViewById(R.id.Array5);


        Lstack1.setVisibility(View.INVISIBLE);
        Lstack2.setVisibility(View.INVISIBLE);
        Lstack3.setVisibility(View.INVISIBLE);
        Lstack4.setVisibility(View.INVISIBLE);
        Lstack5.setVisibility(View.INVISIBLE);

        Array1.setVisibility(View.INVISIBLE);
        Array2.setVisibility(View.INVISIBLE);
        Array3.setVisibility(View.INVISIBLE);
        Array4.setVisibility(View.INVISIBLE);
        Array5.setVisibility(View.INVISIBLE);
    }


    public interface Stack<T> {
        public int size();

        public boolean isEmpty();

        public void push(T key);

        public T pop();

        public T top();
    }


    public class LinkedStack<T> implements Stack<T> {
        private Node<T> top;
        private int size = 0;

        @Override
        public int size() {
            return size;
        }

        @Override
        public boolean isEmpty() {
            if (size == 0) {
                return true;
            }
            return false;
        }

        @Override
        public void push(T key) {
            Node<T> node = new Node<T>(key);
            node.next = this.top;
            this.top = node;
            size++;
            viewStacks(key);
            visibilityStack();

        }

        @Override
        public T pop() {
            T temp;
            if (isEmpty()) {
                pilaText.setText("Pila Vacia...".toString());
                this.size = 0;
                return null;
            } else if (this.size == 1) {
                temp = this.top();
                this.top = null;
                this.size = 0;
                this.visibilityStack();
                return temp;
            }else{

            temp = this.top.value;
            this.top = this.top.next;
            this.size--;
            this.visibilityStack();
            return temp;
            }
        }

        @Override
        public T top() {
            if (isEmpty()) {
                pilaText.setText("Empty Stack".toString());
                return null;
            }
            return (T) this.top.value;
        }

        public void visibilityStack(){
            if(this.size == 1){
                Lstack1.setVisibility(View.VISIBLE);
            }else if(this.size == 2){
                Lstack2.setVisibility(View.VISIBLE);
            }else if(this.size == 3){
                Lstack3.setVisibility(View.VISIBLE);
            }else if(this.size == 4){
                Lstack4.setVisibility(View.VISIBLE);
            }else if(this.size == 5){
                Lstack5.setVisibility(View.VISIBLE);
            }
        }

        public void viewStacks(T key){
            if(this.size == 1){
                Lstack1.setText(String.valueOf(key));
            }else if(this.size == 2){
                Lstack2.setText(String.valueOf(key));
            }else if(this.size == 3){
                Lstack3.setText(String.valueOf(key));
            }else if(this.size == 4){
                Lstack4.setText(String.valueOf(key));
            }else if(this.size == 5){
                Lstack5.setText(String.valueOf(key));
            }
        }
    }

    public class ArrayStack<T> implements Stack<T> {
        T top;
        T[] elementsList;
        int size;
        int nElements;
        Button[] arrayButtons;

        public ArrayStack(int size) {
            this.top = null;
            this.elementsList = (T[]) new Object[5]; //Cast
            this.size = 5;
            this.nElements = -1;
        }

        @Override
        public int size() {
            return this.size;
        }

        public int numberElements(){
            return this.nElements+1;
        }

        @Override
        public boolean isEmpty() {
            if(nElements == -1) return true;
            else return false;
        }

        @Override
        public void push(T key) {
            if(this.numberElements() == size){
                Toast.makeText(ImplementHeap.this, "Array Stack Full", Toast.LENGTH_SHORT).show();
            }else {
                this.elementsList[nElements + 1] = key;
                this.top = this.elementsList[nElements+1];
                this.nElements++;
                this.setText(key);
                this.viewElements();
            }
        }

        @Override
        public T pop() {

            T elementTop = null;
            if(this.isEmpty()){
                Toast.makeText(ImplementHeap.this, "Array Stack is Empty", Toast.LENGTH_SHORT).show();
                this.viewElements();
            } else if(this.nElements == 1){
                elementTop = this.top;
                this.top = null;
                this.elementsList[0] = null;
                this.nElements--;
                this.viewElements();
            }else {
                elementTop = this.top;
                this.elementsList[nElements]=null;
                this.top = this.elementsList[nElements];
                this.nElements--;
                this.viewElements();
            }

            return elementTop;
        }

        @Override
        public T top() {
            return this.top;
        }

        public void viewElements(){
            if(this.numberElements() == 0){
                Array1.setVisibility(View.INVISIBLE);
                Array2.setVisibility(View.INVISIBLE);
                Array3.setVisibility(View.INVISIBLE);
                Array4.setVisibility(View.INVISIBLE);
                Array5.setVisibility(View.INVISIBLE);
            }
            if(this.numberElements() == 1){
                Array1.setVisibility(View.VISIBLE);
                Array2.setVisibility(View.INVISIBLE);
                Array3.setVisibility(View.INVISIBLE);
                Array4.setVisibility(View.INVISIBLE);
                Array5.setVisibility(View.INVISIBLE);
            }
            if(this.numberElements() == 2){
                Array1.setVisibility(View.VISIBLE);
                Array2.setVisibility(View.VISIBLE);
                Array3.setVisibility(View.INVISIBLE);
                Array4.setVisibility(View.INVISIBLE);
                Array5.setVisibility(View.INVISIBLE);
            }
            if(this.numberElements() == 3){
                Array1.setVisibility(View.VISIBLE);
                Array2.setVisibility(View.VISIBLE);
                Array3.setVisibility(View.VISIBLE);
                Array4.setVisibility(View.INVISIBLE);
                Array5.setVisibility(View.INVISIBLE);
            }
            if(this.numberElements() == 4){
                Array1.setVisibility(View.VISIBLE);
                Array2.setVisibility(View.VISIBLE);
                Array3.setVisibility(View.VISIBLE);
                Array4.setVisibility(View.VISIBLE);
                Array5.setVisibility(View.INVISIBLE);
            }
            if(this.numberElements() == 5){
                Array1.setVisibility(View.VISIBLE);
                Array2.setVisibility(View.VISIBLE);
                Array3.setVisibility(View.VISIBLE);
                Array4.setVisibility(View.VISIBLE);
                Array5.setVisibility(View.VISIBLE);
            }
        }

        public void setText(T key) {
            if (this.numberElements() == 1) {
                Array1.setText(String.valueOf(key));
            } else if (this.numberElements() == 2) {
                Array2.setText(String.valueOf(key));
            } else if (this.numberElements() == 3) {
                Array3.setText(String.valueOf(key));
            } else if (this.numberElements() == 4) {
                Array4.setText(String.valueOf(key));
            } else if (this.numberElements() == 5) {
                Array5.setText(String.valueOf(key));
            }
        }

    }

    public void Login(View view){
        Intent login = new Intent(this,MainActivity.class);
        finish();
    }

    public void AddToStack(View view) {
        elementText = (EditText) findViewById(R.id.elementText);
        String dataStack = String.valueOf(elementText.getText());
        linkedStack.push(dataStack);
        arrayStack.push(dataStack);
        sizeOfLS++;
    }

    public void popStack(View view){
        linkedStack.pop();
        arrayStack.pop();
    }

    public void topStack(){
        linkedStack.top();
        arrayStack.top();
    }
}
    // Implementar también en Listas.

