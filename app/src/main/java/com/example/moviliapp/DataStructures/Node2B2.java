package com.example.moviliapp.DataStructures;

import java.io.Serializable;

public class Node2B2 <T> extends Node <T> implements Serializable{

	/*
	28/04/2020 Added Node2B2() and Node2B2(value, next, previous) constructors
	*/
    Node2B2<T> next;
    Node2B2<T> previous;
	
	public Node2B2(){ // Constructor without key
        this.next = null;
		this.previous = null;
    }

    public Node2B2(T value) {
        super(value);
        this.next = null;
		this.previous = null;
    }
	
	public Node2B2(T value, Node2B2<T> next) {
        super(value);
        this.next = next;
		this.previous = null;
    }
	
	public Node2B2(T value, Node2B2<T> next, Node2B2<T> previous) {
        super(value);
        this.next = next;
		this.previous = previous;
    }
	
	
	public T getValue() {
        return value;
    }

    public void setNext(Node2B2<T> next) {
        super.setNext(next);
    }

    public void setPrevious(Node2B2<T> previous) {
        this.previous = previous;
    }
	
	@Override
    public String toString() {
        return super.toString()+"The value of node is" + this.value + ", next is" + this.next.toString() + "and previous is" + this.previous.toString();
    }
}
