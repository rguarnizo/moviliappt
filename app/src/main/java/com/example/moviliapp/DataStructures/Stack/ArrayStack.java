package com.example.moviliapp.DataStructures.Stack;


import java.io.Serializable;

public class ArrayStack<T> implements Stack<T>, Serializable {

    T top;
    T[] arrayStack;
    int size = 0;
    int length;

    public ArrayStack(){
        this.arrayStack = (T[]) new Object[5];
        this.length = 5;
    }

    public ArrayStack(int length){
        this.arrayStack = (T[]) new Object[length];
        this.length = length;
    }



    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if(this.size == 0){
            return true;
        } else return false;
    }

    @Override
    public void push(T key) {

        if(this.size == this.length) {
            //ArrayStack Full;
        }else {
            this.arrayStack[size] = key;
            this.top = this.arrayStack[size];
            this.size++;
        }
    }

    @Override
    public T pop() {
        if(isEmpty()){
            //ArrayStack Empty

            return  null;
        }else if(this.size == 1){
            T temp = this.top;
            this.top = null;
            this.size = 0;

            return  temp;
        }else{
            T temp = this.top;
            this.top = this.arrayStack[this.size-1];
            this.size--;

            return temp;
        }
    }

    @Override
    public T top() {
        return this.top;
    }
}
