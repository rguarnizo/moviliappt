package com.example.moviliapp.DataStructures.Stack;


import com.example.moviliapp.DataStructures.Node;

import java.io.Serializable;

public class LinkedStack <T> implements Stack<T>, Serializable {

        Node top;
        Node tail;

        int size;

        public LinkedStack(){
            this.top = null;
            this.tail= null;
            this.size= 0;
        }

        public int size() {
            return this.size;
        }


        public boolean isEmpty() {
            if(size == 0){
                return true;
            }else return false;
        }


        public void push(T key) {
            Node pushItem = new Node<T>(key);

            if(isEmpty()){
                this.top = pushItem;
                this.tail = pushItem;
            } else{
                pushItem.next = this.top;
                this.top = pushItem;
            }
            this.size++;
        }


        public T pop() {
            Node <T> popNode;
            if(isEmpty()){
                //ArrayStack is empty

                return null;
            } else if(this.size() == 1){
                popNode = this.top;

                this.top = null;
                this.tail = null;
                size--;

                return popNode.value;
            } else{
                popNode = this.top;
                this.top = this.top.next;
                size--;

                return popNode.value;
            }
        }


        public T top() {
            T topValue = (T) this.top.value;
            return topValue;
        }
    }

