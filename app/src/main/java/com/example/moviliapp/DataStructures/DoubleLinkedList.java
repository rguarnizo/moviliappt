package com.example.moviliapp.DataStructures;

import com.example.moviliapp.DataStructures.LinkedList;
import com.example.moviliapp.DataStructures.Node2B2;

import java.util.*;
import java.io.Serializable;

/*
28/04/2020
Se agregaron los métodos
	*append()
	*add()
	*isEmpty()
	*size()
	*get()
	*indexOf()
	*remove()
	*toString()

Se agregó un iterador para cuando sea necesario, con sus respectivos métodos

*/

public class DoubleLinkedList <T> extends  LinkedList<T> implements  Iterable<T>, Serializable{

    Node2B2 <T> head;
    Node2B2 <T> tail;
    int size;

    public DoubleLinkedList(){
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

	public void append ( T value ){
		Node2B2 node = new Node2B2(value, null, null);
		if( this.tail == null ) {
			this.tail = node;
			this.head = this.tail;
			node.previous = null;
		} else {
			node.previous = this.tail;
			tail.next = node;
			tail = node;
		}
		size++;
	}

	@Override
	public void add ( int index, T value ){
		checkIndex(index);

		if ( index == 0 ){
			Node2B2 node = new Node2B2<T>(value, head, null);
			head.previous = node;
			head = node;
		} else {
			Node2B2<T> aux = head;
			for( int i = 0; i< index - 1; i++){
				aux = aux.next; // Find ancestor of the new  node
			}
			Node2B2 newNode = new Node2B2(value, aux.next, aux);
			aux.next.previous = newNode;
			aux.next = newNode;
		}
		size++;
	}

	public boolean isEmpty(){
		return size == 0;
	}

	public int size(){
		return size;
	}

	@Override
	public T get(int index){
		checkIndex(index);

        //Está el elemento cerca de la cola o cerca de la cabeza?(antes o despues de la mediana)
        int aux = size/2;
        if( index < aux ){ //Si está en la primera mitad, es más rapido ir desde la cabeza
            Node2B2<T> currentNode = head;
            for(int i = 0; i < index; i++){
                currentNode = currentNode.next;
            }
            return currentNode.value;
        } else { //En otro caso esta en la segunda mitad y es mas eficiente ir desde la cola
            Node2B2<T> currentNode = tail;
            for(int i = size-1; i > index; i--){
                currentNode = currentNode.next;
            }
            return currentNode.value;
        }
	}

	@Override
	public int indexOf( T value ){
		Node2B2<T> currentNode = head;
		int index = 0;
		while ( currentNode != null && !currentNode.value.equals(value) ){
			currentNode = currentNode.next;
			index++;
		}
		if( currentNode == null ){
			return -1;
		} else {
			return index;
		}
	}

	@Override
	public T remove ( int index ){ //PopBack es remove(size-1);
		checkIndex( index );

		T removedValue;
		if( index == 0 ){
			removedValue = head.value;
			head = head.next;
		} else {
			Node2B2<T> aux = head;
			for( int i=0; i< index - 1; i++ ){
				aux = aux.next;
			}
			removedValue = aux.next.value;
			aux.next = aux.next.next;
		}
		size--;
		return removedValue;
	}

	@Override
	public String toString(){
		StringBuilder s = new StringBuilder("[");

		for( T x : this){
			s.append( Objects.toString(x) ).append(", ");
		}
		if( size > 0 ){
			s.setLength( s.length() - 2 );
		}
		s.append("]");
		return new String(s);
	}

	private class DoubleLinkedListIterator implements Iterator<T>{
		private Node2B2<T> nextNode;

		public DoubleLinkedListIterator(){
			nextNode = head;
		}

		public boolean hasNext(){
			return nextNode != null;
		}

		@Override
		public T next(){
			if( nextNode != null ){
				T valueToReturn = nextNode.value;
				nextNode = nextNode.next;
				return valueToReturn;
			} else {
				throw new NoSuchElementException("No next element.");
			}
		}

		@Override
		public void remove(){
			throw new UnsupportedOperationException("remove not supported.");
		}
	}

	@Override
	public Iterator<T> iterator(){
		return new DoubleLinkedListIterator();
	}



    public T popBack(){
        if(this.size == 0){
            //The DoublyLinkList is empty
            return null;
        }else if(this.size == 1){
            T itemPop = this.head.value;
            this.head = null;
            this.tail = null;
            size--;

            return itemPop;

        }else{
            return null;
        }
    }





}
