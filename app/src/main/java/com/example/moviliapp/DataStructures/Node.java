package com.example.moviliapp.DataStructures;


import java.io.Serializable;

public class Node<T> implements Serializable {

    public T value;
    public Node<T> next;
	
	/*
	28/04/2020
	Added Node() and Node(value, next) constructors
	*/
	
	public Node(){
		this.next = null;
    }

    public Node(T value){
        this.value = value;
		this.next = null;
    }

	public Node(T value, Node<T> next){
        this.value = value;
		this.next = next;
    }
	
    public T getValue() {
        return value;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }


}
