package com.example.moviliapp.DataStructures.Array;

public interface Array<T> {

    int getSize();
    int getLength();
    void push(T key);

}
